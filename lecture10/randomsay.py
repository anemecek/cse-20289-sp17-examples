#!/usr/bin/env python2.7

import os
import random
import sys


CHARACTERS = []
for index, line in enumerate(os.popen('cowsay -l')):
    if index:
        for character in line.split():
            CHARACTERS.append(character)

SELECTED = random.choice(CHARACTERS)
os.system('cowsay -f {}'.format(SELECTED))
