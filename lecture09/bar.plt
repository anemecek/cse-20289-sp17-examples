set terminal png
set grid
set style data histogram
set style fill solid border
set xrange [0:7]
set yrange [0:8]
set boxwidth 0.95 relative

plot 'counts.txt' with boxes lt rgb "blue" notitle
